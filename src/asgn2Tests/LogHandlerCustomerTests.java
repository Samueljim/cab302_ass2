package asgn2Tests;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.*;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Restaurant.*;

/**
 * A class that tests the methods relating to the creation of Customer objects
 * in the asgn2Restaurant.LogHander class.
 *
 * @author Samuel Henry
 */
public class LogHandlerCustomerTests {
	private DriverDeliveryCustomer testDriverDeliveryCustomer;
	private Customer testCustomer;

	@Before
	public void setUp() throws Exception {

	}

	// The createPizza test should return a pizza object the same as as every
	// other pizza object
	@Test
	public void createCustomerObjectTest() throws Exception {
		String line = "21:01:00,21:25:00,testy test,0123456789,DVC,-3,-2,PZL,3";
		testCustomer = LogHandler.createCustomer(line);
		testDriverDeliveryCustomer = new DriverDeliveryCustomer("testy test", "0123456789", -3, -2);
		assertTrue(testDriverDeliveryCustomer.equals(testCustomer));
	}

	// If the line is invalid then there should be an exception
	@Test(expected = LogHandlerException.class)
	public void createCustomerLogExceptionTest() throws Exception {
		String line = "Hey there";
		testCustomer = LogHandler.createCustomer(line);
	}

	// Test that an exception is thrown if the index is off 
	@Test(expected = LogHandlerException.class)
	public void createCustomerLogWrongIndexExceptionTest() throws Exception {
		String line = "wrong,21:01:00,21:25:00,testy test,0123456789,PUC,-3,-2,PZL,3";
		testCustomer = LogHandler.createCustomer(line);
	}

	//The customer exceptions should be handled
	@Test(expected = CustomerException.class)
	public void createCustomerExceptionTest() throws Exception {
		String line = "21:26:00,21:25:00,testy test,0123456789,PUC,-3,-2,PZL,3";
		testCustomer = LogHandler.createCustomer(line);
	}

}
