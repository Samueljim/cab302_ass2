package asgn2Tests;

import asgn2Customers.*;
import asgn2Exceptions.CustomerException;
import org.junit.Before;
import org.junit.Test;
import org.junit.After;

import static org.junit.Assert.*;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 *
 * @author Samuel Henry
 *
 */
public class CustomerFactoryTests {
	
	private DroneDeliveryCustomer testDroneDeliveryCustomer;
	private DriverDeliveryCustomer testDriverDeliveryCustomer;
	private PickUpCustomer testPickUpCustomer; 
	private Customer testCustomer;
	
	
	// test that an exception is thrown then the type isn't valid
	@Test(expected = CustomerException.class)
	public void notValidTypeExceptionTest1() throws Exception {
		CustomerFactory.getCustomer("JFK", "Samuel", "0594735829", 4, 2);
	}
	
	@Test(expected = CustomerException.class)
	public void notValidTypeExceptionTest2() throws Exception {
		CustomerFactory.getCustomer("DNV", "Samuel", "0594735829", 4, 2);
	}
	// tests that the Customer isn't a pizza :P
	@Test(expected = CustomerException.class)
	public void notValidTypeExceptionTest3() throws Exception {
		CustomerFactory.getCustomer("Margarita", "Samuel", "0594735829", 4, 2);
	}

	@Test(expected = CustomerException.class)
	public void notValidTypeExceptionTest4() throws Exception {
		CustomerFactory.getCustomer("PZM", "Samuel", "0594735829", 4, 2);
	}
	//tests that getCustomer returns a Customer type object 
	@Test
	public void objectReturnedDriverTest() throws Exception {
		testCustomer = CustomerFactory.getCustomer("DVC", "Sam", "0345372677", -3, -2);
		testDriverDeliveryCustomer = new DriverDeliveryCustomer("Sam", "0345372677", -3, -2);
		assertEquals(testCustomer.getCustomerType(), "Delivery");
		assertTrue(testDriverDeliveryCustomer.equals(testCustomer));
	}
	@Test
	public void objectReturnedDroneTest() throws Exception {
		testCustomer = CustomerFactory.getCustomer("DNC", "Sam", "0345372677", -3, -2);
		testDroneDeliveryCustomer = new DroneDeliveryCustomer("Sam", "0345372677", -3, -2);
		assertEquals(testCustomer.getCustomerType(), "Drone");
		assertTrue(testDroneDeliveryCustomer.equals(testCustomer));
	}
	@Test
	public void objectReturnedPickUpTest() throws Exception {
		testCustomer = CustomerFactory.getCustomer("PUC", "Sam", "0345372677", 0, 0);
		testPickUpCustomer = new PickUpCustomer("Sam", "0345372677", 0, 0);
		assertEquals(testCustomer.getCustomerType(), "PickUp");
		assertTrue(testPickUpCustomer.equals(testCustomer));
	}
	
}
