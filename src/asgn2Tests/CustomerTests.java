package asgn2Tests;

//import asgn2Customers.Customer;
import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer,
 * asgn2Customers.DriverDeliveryCustomer, asgn2Customers.DroneDeliveryCustomer
 * classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer
 * should be used to test the functionality of the asgn2Customers.Customer
 * abstract class.
 *
 * @author Samuel Henry
 */
public class CustomerTests {

	private DriverDeliveryCustomer testDeliveryCustomer;
	private PickUpCustomer testPickUpCustomer;
	private DroneDeliveryCustomer testDroneDeliveryCustomer;

	// the before method is making one of each type of customer before testing
	@Before
	public void setUp() throws CustomerException {
		testDeliveryCustomer = new DriverDeliveryCustomer("Bob", "0405433639", 4, 4);
		testPickUpCustomer = new PickUpCustomer("Dave", "0454544435", 0, 0);
		testDroneDeliveryCustomer = new DroneDeliveryCustomer("Sam", "0345372677", 3, 3);
	}

	// this test checks that the input of x4 and y4 return the expected output
	@Test
	public void getDeliveryDistanceTest() throws Exception {
		double DeliveryDistance = testDeliveryCustomer.getDeliveryDistance();
		assertEquals(DeliveryDistance, 8, 0);
	}

	// test that different locations delivery distance is calculated correctly
	@Test
	public void getDeliveryDistanceCalculationTest() throws Exception {
		int[] answers = { 10, 5, 9, 20, 15, 10 };
		int[] xInput = { 5, 2, -4, 10, -6, -5 };
		int[] yInput = { 5, 3, 5, -10, -9, -5 };
		for (int iterator = 0; iterator < answers.length; iterator++) {
			DriverDeliveryCustomer testDeliveryCustomer1 = new DriverDeliveryCustomer("Bob", "0405433639",
					xInput[iterator], yInput[iterator]);
			double DeliveryDistance = testDeliveryCustomer1.getDeliveryDistance();
			assertEquals(answers[iterator], DeliveryDistance, 0);
		}
	}

	// if the delivery distance is outside of 10 blocks in any direction then
	// there should be a exception
	@Test(expected = CustomerException.class)
	public void getDeliveryDistanceExceptionTest1() throws Exception {
		new DriverDeliveryCustomer("Mike", "0454544435", 11, 0);
	}

	@Test(expected = CustomerException.class)
	public void getDeliveryDistanceExceptionTest2() throws Exception {
		new DriverDeliveryCustomer("Mike", "0454544435", 0, 11);
	}

	@Test(expected = CustomerException.class)
	public void getDeliveryDistanceExceptionTest3() throws Exception {
		new DriverDeliveryCustomer("Mike", "0454544435", 0, -11);
	}

	@Test(expected = CustomerException.class)
	public void getDeliveryDistanceExceptionTest4() throws Exception {
		new DriverDeliveryCustomer("Mike", "0454544435", -11, 0);
	}

	@Test(expected = CustomerException.class)
	public void getDeliveryDistanceExceptionTest5() throws Exception {
		new DriverDeliveryCustomer("Mike", "0454544435", 50, -10);
	}

	// a delivery distance for all pick up types should zero
	@Test
	public void getPickDistanceTest() throws Exception {
		double DeliveryDistance = testPickUpCustomer.getDeliveryDistance();
		assertTrue(DeliveryDistance == 0);
	}

	// If the pickup class gets a x and a y of something other than zero then
	// there should be an exception
	@Test(expected = CustomerException.class)
	public void getPickDistanceExceptionTest1() throws Exception {
		new PickUpCustomer("Mike", "0454544435", 1, 0);
	}

	@Test(expected = CustomerException.class)
	public void getPickDistanceExceptionTest2() throws Exception {
		new PickUpCustomer("Mike", "0454544435", 0, 1);
	}

	@Test(expected = CustomerException.class)
	public void getPickDistanceExceptionTest3() throws Exception {
		new PickUpCustomer("Mike", "0454544435", -10, -10);
	}

	@Test(expected = CustomerException.class)
	public void getPickDistanceExceptionTest4() throws Exception {
		new PickUpCustomer("Mike", "0454544435", 3, 4);
	}

	// tests the drone delivery distance
	@Test
	public void getDroneDistanceTest() throws Exception {
		double deliveryDroneDistance = testDroneDeliveryCustomer.getDeliveryDistance();
		assertEquals(4.24, deliveryDroneDistance, 0);
	}

	@Test
	public void getDroneDistanceCalculationTest() throws Exception {
		double[] answers = {5, 5, 5, 5, 2.83, 7.07};
		int[] xInput = {3, -3, -3, 3, 2, -5};
		int[] yInput = {4, -4 , 4, -4, 2, 5};
		for (int iterator = 0; iterator < answers.length; iterator++) {
			DroneDeliveryCustomer testDroneCustomer = new DroneDeliveryCustomer("Name", "0405433639", xInput[iterator],
					yInput[iterator]);
			double deliveryDroneDistance = testDroneCustomer.getDeliveryDistance();
			assertEquals(answers[iterator], deliveryDroneDistance, 0);
		}
	}

	// test that getName will return the right name
	@Test
	public void getNameTest() throws Exception {
		String name = testDeliveryCustomer.getName();
		assertEquals(name, "Bob");
	}

	// check that the name is shorter than 20
	@Test
	public void getNameTooLongTest() throws Exception {
		String name = testDeliveryCustomer.getName();
		int length = name.length();
		assertTrue(length <= 20);
	}

	// the name should be allowed at 20 long but not longer
	@Test
	public void getNameMaxLengthTest1() throws Exception {
		DriverDeliveryCustomer customer = new DriverDeliveryCustomer("nameThatIsTwentyLong", "0454544435", 3, 4);
		String name = customer.getName();
		int length = name.length();
		assertTrue(length <= 20);
	}

	// if the name is more than 20 letter long then it's not allowed
	@Test(expected = CustomerException.class)
	public void getNameMaxLengthTest2() throws Exception {
		new DriverDeliveryCustomer("nameIsMoreThanTwenty!", "0454544435", 3, 4);
	}

	// check length is longer than zero
	@Test
	public void getNameTooShortTest() throws Exception {
		String name = testDeliveryCustomer.getName();
		int length = name.length();
		assertTrue(length > 0);
	}

	// the name can't be just spaces or null
	@Test(expected = CustomerException.class)
	public void nameCantBeBlankTest() throws Exception {
		String name = testDeliveryCustomer.getName();
		String toCompare = "";
		for (int iterator = 0; iterator <= name.length(); iterator++) {
			new DriverDeliveryCustomer(toCompare, "1234567890", 1, 1);
			toCompare += "";
		}
	}

	@Test(expected = CustomerException.class)
	public void justSpacesExceptionTest() throws Exception {
		new DriverDeliveryCustomer("   ", "1234567890", 1, 1);
	}

	// the mobile number needs to be 10 long
	@Test
	public void getMobileNumberLengthTest() throws Exception {
		String testNumber = testDeliveryCustomer.getMobileNumber();
		int length = testNumber.length();
		assertEquals(length, 10);
	}

	// test that starting without a leading zero throws exception
	@Test
	public void getMobileNumberStartInZeroTest() throws Exception {
		String testNumber = testDeliveryCustomer.getMobileNumber();
		char firstChar = testNumber.toCharArray()[0];
		String string = String.valueOf(firstChar);
		assertEquals(string, "0");
	}

	@Test
	public void mobileNumberTest() throws Exception {
		String[] numbers = { "0405433639", "0403040303", "0934818243", "0123456789", "0987654321" };
		for (String number : numbers) {
			DriverDeliveryCustomer testDriverCustomer = new DriverDeliveryCustomer("Test man", number, 1, 1);
			String mobileNumber = testDriverCustomer.getMobileNumber();
			assertEquals(mobileNumber, number);
		}
	}

	// a exception should be thrown if the number dosn't start with a zero
	@Test(expected = CustomerException.class)
	public void mobileNumberWithoutZeroExceptionTest() throws Exception {
		new DriverDeliveryCustomer("Test man", "1234567890", 1, 1);
	}

	// test that it returns a mobile numbers without letters in it
	@Test(expected = CustomerException.class)
	public void mobileWithLetterExceptionTest() throws Exception {
		new DriverDeliveryCustomer("Test man", "012345678f", 1, 1);
	}

	// a mobile number should be only 10 characters long
	@Test(expected = CustomerException.class)
	public void mobileTooShortTest() throws Exception {
		new DriverDeliveryCustomer("Test man", "012345678", 1, 1);
	}

	@Test(expected = CustomerException.class)
	public void mobileTooLongTest() throws Exception {
		new DriverDeliveryCustomer("Test man", "01234567899", 1, 1);
	}

	// the getCustomerType function should return the right type
	@Test
	public void getCustomerTypeTest1() throws Exception {
		String testType = testDeliveryCustomer.getCustomerType();
		assertEquals(testType, "Delivery");
	}

	// the getCustomerType function should return the right type
	@Test
	public void getCustomerTypeTest2() throws Exception {
		String testType = testDroneDeliveryCustomer.getCustomerType();
		assertEquals(testType, "Drone");
	}

	// the getCustomerType function should return the right type
	@Test
	public void getCustomerTypeTest3() throws Exception {
		String testType = testPickUpCustomer.getCustomerType();
		assertEquals(testType, "PickUp");
	}

	// should return the right x value
	@Test
	public void getLocationXTest() throws Exception {
		int testX = testDeliveryCustomer.getLocationX();
		assertEquals(testX, 4);
	}

	// should return the right y value
	@Test
	public void getLocationYTest() throws Exception {
		int testY = testDeliveryCustomer.getLocationY();
		assertEquals(testY, 4);
	}

}
