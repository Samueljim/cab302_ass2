package asgn2Wizards;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import asgn2GUIs.PizzaGUI;


/**
 * This class is the �entry point� to the rest of the system and provides a public static void main method. 
 * At the moment, this just calls the asgn2GUIs.PizzaGUI class. You can probably leave the class as it is,
 *  however, you must make sure that it is the one and only entry point to the rest of the system. 
 *
 *  @author Samuel Henry and Person B
 */

public class PizzaWizard {

	
	/**
	 * Creates an instance of PizzaWizard
	 */
	public PizzaWizard() {
	}
	/**
	 * The entry point to the rest of the system.
	 * @param args Command Line Arguments
	 */
	public static void main(String[] args) {
//		PizzaGUI swingContainer = new PizzaGUI("Pizza Place");
//		swingContainer.run();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
//				try {
					PizzaGUI window = new PizzaGUI("Pizza Place");
					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
			}
		});
	}

}
