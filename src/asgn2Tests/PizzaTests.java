package asgn2Tests;

import asgn2Pizzas.*;
import org.junit.Before;
import org.junit.Test;

import asgn2Customers.DriverDeliveryCustomer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.PizzaException;

import java.time.LocalTime;
import java.util.ArrayList;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import java.time.LocalTime;

/**
 * A class that that tests the asgn2Pizzas.MargheritaPizza,
 * asgn2Pizzas.VegetarianPizza, asgn2Pizzas.MeatLoversPizza classes. Note that
 * an instance of asgn2Pizzas.MeatLoversPizza should be used to test the
 * functionality of the asgn2Pizzas.Pizza abstract class.
 *
 * @author Hayden Muller
 *
 */
public class PizzaTests {
	MargheritaPizza newMargheritaPizzaOne;
	MargheritaPizza newMargheritaPizzaTwo;

	VegetarianPizza newVegetarianPizzaOne;
	VegetarianPizza newVegetarianPizzaTwo;

	MeatLoversPizza newMeatLoversPizzaOne;
	MeatLoversPizza newMeatLoversPizzaTwo;
	MeatLoversPizza newMeatLoversPizzaThree;
	MeatLoversPizza newMeatLoversPizzaFour;
	MeatLoversPizza newMeatLoversPizzaFive;

	@Before
	public void setUp() throws Exception {
		
		this.newMeatLoversPizzaTwo = new MeatLoversPizza(1, LocalTime.of(19,20), LocalTime.of(19,30));
		this.newMeatLoversPizzaThree = new MeatLoversPizza(5, LocalTime.of(19, 20), LocalTime.of(19,30));
		this.newMeatLoversPizzaFour = new MeatLoversPizza(10, LocalTime.of(19, 20), LocalTime.of(19,30));
		this.newVegetarianPizzaOne = new VegetarianPizza(1, LocalTime.of(19, 20), LocalTime.of(19, 30));
		this.newMargheritaPizzaOne = new MargheritaPizza(1, LocalTime.of(19, 20), LocalTime.of(19, 30));
		
		
		
	}

	// Tests thats the cost calculations of the pizza's are correct.
	@Test
	public void calculateCostPerPizza() throws Exception {
		double pizzaCostOne = newMeatLoversPizzaTwo.getCostPerPizza();
		assertTrue(pizzaCostOne == 5);
	}

	// Tests that the price per pizza value is correct.
	@Test
	public void getPricePerPizzaTest() throws Exception {
		double pizzaPriceOne = newMeatLoversPizzaTwo.getPricePerPizza();
		assertTrue(pizzaPriceOne == 12);
	}

	// Tests that the price of the order value returned is correct.
	@Test
	public void getOrderCostTest() throws Exception {
		double pizzaOrderCostTwo = newMeatLoversPizzaTwo.getOrderCost();
		double pizzaOrderCostThree = newMeatLoversPizzaThree.getOrderCost();
		double pizzaOrderCostFour = newMeatLoversPizzaFour.getOrderCost();
		assertTrue(pizzaOrderCostTwo == 5);
		assertTrue(pizzaOrderCostThree == 25);
		assertTrue(pizzaOrderCostFour == 50);

	}

	// Tests that the order price value returned is correct
	@Test
	public void getOrderPriceTest() throws Exception {
		double pizzaOrderPriceTwo = newMeatLoversPizzaTwo.getOrderPrice();
		double pizzaOrderPriceThree = newMeatLoversPizzaThree.getOrderPrice();
		double pizzaOrderPriceFour = newMeatLoversPizzaFour.getOrderPrice();
		assertTrue(pizzaOrderPriceTwo == 12);
		assertTrue(pizzaOrderPriceThree == 60);
		assertTrue(pizzaOrderPriceFour == 120);
	}

	// Tests that profit value returned is correct
	@Test
	public void getOrderProfitTest() throws Exception {
		double pizzaOrderPriceTwo = newMeatLoversPizzaTwo.getOrderProfit();
		double pizzaOrderPriceThree = newMeatLoversPizzaThree.getOrderProfit();
		double pizzaOrderPriceFour = newMeatLoversPizzaFour.getOrderProfit();
		assertTrue(pizzaOrderPriceTwo == 7);
		assertTrue(pizzaOrderPriceThree == 35);
		assertTrue(pizzaOrderPriceFour == 70);
	}

	// Tests that returned toppings for each pizza.
	@Test
	public void containsToppingTest() throws Exception {
		// Doesn't have these toppings
		assertFalse(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.CAPSICUM));
		assertFalse(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.MUSHROOM));
		assertFalse(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.EGGPLANT));
		//Has these toppings.
		assertTrue(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.BACON));
		assertTrue(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.CHEESE));
		assertTrue(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.PEPPERONI));
		assertTrue(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.SALAMI));
		assertTrue(newMeatLoversPizzaTwo.containsTopping(PizzaTopping.TOMATO));
	}

	// Tests the quantity returned is correct.
	@Test
	public void getQuantityTest() throws Exception {
		double pizzaOrderSizeTwo = newMeatLoversPizzaTwo.getQuantity();
		double pizzaOrgerSizeThree = newMeatLoversPizzaThree.getQuantity();
		double pizzaOrgerSizeFour = newMeatLoversPizzaFour.getQuantity();
		assertTrue(pizzaOrderSizeTwo == 1);
		assertTrue(pizzaOrgerSizeThree == 5);
		assertTrue(pizzaOrgerSizeFour == 10);
	}

	// Tests that the type of pizza returned is correct.
	@Test
	public void getPizzaTypeMeatLovers() throws Exception {
		String pizzaType = newMeatLoversPizzaTwo.getPizzaType();
		assertEquals(pizzaType, "Meat Lovers");
	}

	// Tests that the type of pizza returned is correct.
	@Test
	public void getPizzaTypeMargherita() throws Exception {
		String pizzaType = newMargheritaPizzaOne.getPizzaType();
		assertEquals(pizzaType, "Margherita");
	}

	// Tests that the type of pizza returned is correct.
	@Test
	public void getPizzaTypeVegetarian() throws Exception {
		String pizzaType = newVegetarianPizzaOne.getPizzaType();
		assertEquals(pizzaType, "Vegetarian");
	}

	// Tests that an exception isn't thrown when the pizza is 59 minutes, 59
	// seconds old.
	@Test
	public void pizzaOldSecondsTest() throws Exception {
		MeatLoversPizza pizzaOldSecondsTest = new MeatLoversPizza(1, LocalTime.of(19, 20, 30),
				LocalTime.of(20, 20, 29));
		int numberOfPizzas = pizzaOldSecondsTest.getQuantity();
		assertEquals(numberOfPizzas, 1);
	}

	// Tests that an exception isn't thrown when the pizza is 10 minutes, 1
	// second old.
	@Test
	public void notCookedSecondsTest() throws Exception {
		MeatLoversPizza notCookedSecondsTest = new MeatLoversPizza(1, LocalTime.of(19, 20, 30),
				LocalTime.of(19, 30, 31));
		int numberOfPizzas = notCookedSecondsTest.getQuantity();
		assertEquals(numberOfPizzas, 1);
	}

	// Throws exception for when an order's size is too small
	@Test(expected = PizzaException.class)
	public void orderSizeSmallTest() throws Exception {
		new MeatLoversPizza(0, LocalTime.of(19, 20), LocalTime.of(19, 40));
	}

	// Throws exception for when an order's size is too large
	@Test(expected = PizzaException.class)
	public void orderSizeLargeTest() throws Exception {
		new MeatLoversPizza(11, LocalTime.of(19, 20), LocalTime.of(19, 40));
	}

	// Throws exception for when a pizza is too old
	@Test(expected = PizzaException.class)
	public void pizzaOldTest() throws Exception {
		new MeatLoversPizza(1, LocalTime.of(19, 20, 00), LocalTime.of(20, 20, 00));
	}

	// Throws exception for when a pizza is delivered before it is ordered
	@Test(expected = PizzaException.class)
	public void deliverBeforeOrderTest() throws Exception {
		new MeatLoversPizza(1, LocalTime.of(19, 20), LocalTime.of(19, 10));
	}

	// Throws exception for when a pizza is not fully cooked.
	@Test(expected = PizzaException.class)
	public void notCookedTest() throws Exception {
		new MeatLoversPizza(11, LocalTime.of(19, 20), LocalTime.of(19, 30));
	}

	// Throws exception for when a pizza is ordered before opening time.
	@Test(expected = PizzaException.class)
	public void orderBeforeOpenTest() throws Exception {
		new MeatLoversPizza(11, LocalTime.of(18, 59, 59), LocalTime.of(19, 30, 00));
	}

	// Throws exception for when a pizza is ordered after closing time.
	@Test(expected = PizzaException.class)
	public void orderAfterShutTest() throws Exception {
		new MeatLoversPizza(11, LocalTime.of(23, 01, 00), LocalTime.of(23, 30, 00));
	}


}