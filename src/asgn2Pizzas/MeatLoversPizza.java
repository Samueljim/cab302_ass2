package asgn2Pizzas;

import asgn2Exceptions.PizzaException;

import java.time.LocalTime;

/**
 * A class that represents a meat lovers pizza made at the Pizza Palace
 * restaurant. The meat lovers pizza has certain toppings listed in Section 5.1
 * of the Assignment Specification Document. A description of the class's fields
 * and their constraints is provided in Section 5.1 of the Assignment
 * Specification.
 *
 * @author Samuel Henry
 */
public class MeatLoversPizza extends Pizza {
	private static double priceOfPizza = 12;
	private static String pizzaType = "Meat Lovers";

	/**
	 * This class represents a meat lovers pizza made at the Pizza Palace
	 * restaurant. The meat lovers pizza has certain toppings listed in Section
	 * 5.1 of the Assignment Specification Document. A description of the
	 * class's fields and their constraints is provided in Section 5.1 of the
	 * Assignment Specification. A PizzaException is thrown if the any of the
	 * constraints listed in Section 5.1 of the Assignment Specification are
	 * violated.
	 * <p>
	 * <P>
	 * PRE: TRUE
	 * <P>
	 * POST: All field values including the cost per pizza are set
	 *
	 * @param quantity
	 *            - The number of pizzas ordered
	 * @param orderTime
	 *            - The time that the pizza order was made and sent to the
	 *            kitchen
	 * @param deliveryTime
	 *            - The time that the pizza was delivered to the customer
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */

	public MeatLoversPizza(int quantity, LocalTime orderTime, LocalTime deliveryTime) throws PizzaException {
		super(quantity, orderTime, deliveryTime, pizzaType, priceOfPizza);

		ingredients.add(PizzaTopping.CHEESE);
		ingredients.add(PizzaTopping.PEPPERONI);
		ingredients.add(PizzaTopping.SALAMI);
		ingredients.add(PizzaTopping.BACON);
		ingredients.add(PizzaTopping.TOMATO);

		// Check if the right toppings are of a on this pizza
		if (containsTopping(PizzaTopping.EGGPLANT) || containsTopping(PizzaTopping.CAPSICUM)
				|| containsTopping(PizzaTopping.MUSHROOM)) {
			throw new PizzaException("Toppings are not right");
		}

	}

}
