package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentSkipListSet;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.*;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;

//import asgn2Exceptions.CustomerException;

/**
 * A class that that tests the methods relating to the handling of Customer
 * objects in the asgn2Restaurant.PizzaRestaurant class as well as processLog
 * and resetDetails.
 * 
 * @author Samuel Henry
 */
public class RestaurantCustomerTests {

	ArrayList<Customer> Customer;

	Customer testCustomer;
	PizzaRestaurant restaurantTester;

	@Before
	public void setUp() throws CustomerException, PizzaException, LogHandlerException {
		restaurantTester = new PizzaRestaurant();
		testCustomer = new DriverDeliveryCustomer("Casey Jones", "0123456789", 5, 5);
		restaurantTester.processLog("20170101.txt");
	}
//	test that getCustomerByIndex returns an object of the right format
	@Test 
	public void getCustomerSizeTest() throws Exception {
		double size = 3;
		assertEquals(size, restaurantTester.getNumCustomerOrders(), 0);
	}
	//test that getCustomerByIndex works
	@Test 
	public void getCustomerByIndexTest() throws Exception {
		restaurantTester.processLog("20170101.txt");
		assertEquals(testCustomer, 	restaurantTester.getCustomerByIndex(0));
	}

	//test that the entire prosses log works by returning a Delivery distance from a file and making sure
	@Test
	public void getTotalDistanceTest() throws Exception {
		double testTotalDistance = restaurantTester.getTotalDeliveryDistance();
		double answer = 15.0;
		assertEquals(answer, testTotalDistance, 0);
	}
//	test to see if reset works 
	@Test
	public void resetTest() throws Exception {
		restaurantTester.resetDetails();
		int size = restaurantTester.getNumCustomerOrders();
		int none = 0;
		assertEquals(none, size);
	}
//	if the log isn't real it should fail
	@Test (expected = LogHandlerException.class)
	public void blankLogTestException() throws Exception {
		restaurantTester.processLog("log101.txt");
	}
	//if the log dosn't have the right number of items on then it should throw an error 
	@Test (expected = LogHandlerException.class)
	public void logHandlerExceptionTest() throws Exception {
		restaurantTester.processLog("error.txt");
	}
	
//	test if getustomerByIndex Exception is thrown 
	@Test (expected = CustomerException.class)
	public void getCustomerByIndexExceptionTest() throws Exception {
		assertEquals(testCustomer, 	restaurantTester.getCustomerByIndex(4));
	}
}
