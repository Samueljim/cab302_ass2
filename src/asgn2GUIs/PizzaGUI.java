package asgn2GUIs;

import java.io.File;


import javax.swing.JLabel;



import javax.swing.JFrame;

import asgn2Restaurant.PizzaRestaurant;
import java.awt.*;
import javax.swing.*;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;

import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;


/**
 * This class is the graphical user interface for the rest of the system.
 * Currently it working class which extends JFrame and implements Runnable and
 * ActionLister. It contains an instance of an asgn2Restaurant.PizzaRestaurant
 * object which is used to interact with the rest of the system.
 *
 *
 * @author Samuel Henry and Hayden Muller
 *
 */
public class PizzaGUI {
	
	private PizzaRestaurant restaurant;
	public JFrame frame;

	private String textFieldLabel;
	private String buttonLabel;

	private Pizza pizza;
	private Customer customer;
	private String name;
	private String mobile;
	private String deliveryType;
	private int locationX;
	private int locationY;
	private double deliveryDistance;
	private String type;
	private int quantity;
	private double price;
	private double cost;
	private double profit;

	private int currentIndex = 0;
	private boolean hasFile = false;

	// ComboBox
	JComboBox comboBox = new JComboBox();

	// JLabel Titles.
	JLabel pizzaOrderIndexTitle = new JLabel("Pizza Order");
	JLabel nameTitle = new JLabel("Recipient Name");
	JLabel mobileTitle = new JLabel("Recipient Mobile");
	JLabel deliveryTypeTitle = new JLabel("Order Type");
	JLabel locationXTitle = new JLabel("Location X");
	JLabel locationYTitle = new JLabel("Location Y");
	JLabel deliveryDistanceTitle = new JLabel("Delivery Distance");
	JLabel pizzaTypeTitle = new JLabel("Pizza Type");
	JLabel pizzaQuanttyTitle = new JLabel("Pizza Quantity");
	JLabel orderPriceTitle = new JLabel("Order Price");
	JLabel orderCostTitle = new JLabel("Order Cost");
	JLabel orderProfitTitle = new JLabel("Order Profit");
	JLabel totalProfitTitle = new JLabel("Total Profit");
	JLabel totalDistanceTitle = new JLabel("Total Distance Traveled");

	// JLabel
	JLabel userInputLabel = new JLabel("Please Select a log file:");
	JLabel lblError = new JLabel("Errors: No Errors.");
	JLabel pizzaOrderIndexLabel = new JLabel("--");
	JLabel nameLabel = new JLabel("--");
	JLabel mobileLabel = new JLabel("--");
	JLabel deliveryTypeLabel = new JLabel("--");
	JLabel locationXLabel = new JLabel("--");
	JLabel locationYLabel = new JLabel("--");
	JLabel deliveryDistanceLabel = new JLabel("--");
	JLabel pizzaTypeLabel = new JLabel("--");
	JLabel pizzaQuantityLabel = new JLabel("--");
	JLabel orderPriceLabel = new JLabel("--");
	JLabel orderCostLabel = new JLabel("--");
	JLabel orderProfitLabel = new JLabel("--");
	JLabel totalProfitLabel = new JLabel("--");
	JLabel totalDistanceLabel = new JLabel("--");

	// Buttons
	private Button nextButton;
	private Button prevButton;
	private Button displayButton;
	private Button calButton;
	private Button resetButton;

	/**
	 * Creates a new Pizza GUI with the specified title
	 * 
	 * @param title
	 *            - The title for the super type JFrame
	 */
	public PizzaGUI(String title) {
		
		restaurant = new PizzaRestaurant();
		frame = new JFrame();
		frame.getContentPane().setFont(new Font("Segoe UI Light", Font.PLAIN, 11));
		frame.setTitle(title);
		frame.setBounds(100, 100, 292, 619);
		frame.setBounds(100, 100, 287, 619);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// center on screen
		frame.setLocationRelativeTo(null);
		comboBox.addItem(null);
		File[] listOfFiles = new File("./logs/").listFiles();

		for (File file : listOfFiles) {
			comboBox.addItem(file.getName());
		}
		
				// Buttons
				nextButton = new Button("Next Order");
				
						nextButton.addActionListener(new ActionListener() {
							@Override
							public void actionPerformed(ActionEvent e) {
								checkNextIndex();
								System.out.println("Next Button Clicked");
							}
						});
						nextButton.setEnabled(false);
		prevButton = new Button("Previous Order");
		prevButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				checkPrevIndex();
				System.out.println("Previous Button Clicked");
			}
		});
		prevButton.setEnabled(false);
		displayButton = new Button("Display Log");
		displayButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				displayValues(0);
				lblError.setText("Displaying log");
			}
		});
		calButton = new Button("Calculate Total");
		calButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				calTotal();
				userInputLabel.setText("Calculate Total Clicked");
			}
		});
		calButton.setEnabled(false);
		displayButton.setEnabled(false);
		resetButton = new Button("Reset Button");
		resetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearValues();
				userInputLabel.setText("Pick a new log file");
				lblError.setText("Reset Button Clicked");
			}
		});
		resetButton.setEnabled(false);
		lblError.setHorizontalAlignment(SwingConstants.LEFT);
		lblError.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 11));
						
								comboBox.addItem(null);
								comboBox.addItemListener(new ItemListener() {
									@Override
									public void itemStateChanged(ItemEvent event) {
										if (event.getStateChange() == ItemEvent.SELECTED) {
											String selected = comboBox.getSelectedItem().toString();
											String error = "";

											try {
												restaurant.processLog(selected);
//												pizza = restaurant.getPizzaByIndex(0);
//												customer = restaurant.getCustomerByIndex(0);
//												name = customer.getName();
//												mobile = customer.getMobileNumber();
//												deliveryType = customer.getCustomerType();
//												locationX = customer.getLocationX();
//												locationY = customer.getLocationY();
//												deliveryDistance = customer.getDeliveryDistance();
//												type = pizza.getPizzaType();\
//												quantity = pizza.getQuantity();
//												price = pizza.getOrderPrice();
//												cost = pizza.getOrderCost();
//												profit = pizza.getOrderProfit();
												displayButton.setEnabled(true);
												resetButton.setEnabled(true);
											
									

											} catch (Exception e) {
												error = e.getMessage();
												lblError.setText("Error: " + e.hashCode());
												JOptionPane.showMessageDialog(null, new JLabel(error));
												displayButton.setEnabled(false);
												nextButton.setEnabled(false);
												prevButton.setEnabled(false);
												calButton.setEnabled(false);
											}
											frame.setTitle(selected + " -- " + title);
//											lblError.setPreferredSize(new Dimension(50, 50));
//											pizzaOrderIndexLabel.setText("1");
//											nameLabel.setText(String.valueOf(name));
//											mobileLabel.setText(String.valueOf(mobile));
//											deliveryTypeLabel.setText(String.valueOf(deliveryType));
//											locationXLabel.setText(String.valueOf(locationX));
//											locationYLabel.setText(String.valueOf(locationY));
//											deliveryDistanceLabel.setText(String.valueOf(deliveryDistance));
//											pizzaTypeLabel.setText(String.valueOf(type));
//											pizzaQuantityLabel.setText(String.valueOf(quantity));
//											orderPriceLabel.setText(String.valueOf(price));
//											orderCostLabel.setText(String.valueOf(cost));
//											orderProfitLabel.setText(String.valueOf(profit));
//											nextButton.setEnabled(true);
//											prevButton.setEnabled(true);
//											calDistButton.setEnabled(true);
//											calProfButton.setEnabled(true);
//											resetButton.setEnabled(true);
											hasFile = true;

										}
									}
								});
								
										comboBox.setFont(new Font("Segoe UI", Font.BOLD, 12));
										comboBox.setBackground(Color.LIGHT_GRAY);
										
										JLabel lblLogFileViewer = new JLabel("Log file viewer");
										lblLogFileViewer.setFont(new Font("Microsoft JhengHei UI", Font.PLAIN, 25));
										GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
										groupLayout.setHorizontalGroup(
											groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(lblLogFileViewer, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
													.addGap(103))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(pizzaOrderIndexTitle)
													.addGap(97)
													.addComponent(pizzaOrderIndexLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(nameTitle)
													.addGap(70)
													.addComponent(nameLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(mobileTitle)
													.addGap(68)
													.addComponent(mobileLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(deliveryTypeTitle)
													.addGap(99)
													.addComponent(deliveryTypeLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(locationXTitle)
													.addGap(102)
													.addComponent(locationXLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(locationYTitle)
													.addGap(102)
													.addComponent(locationYLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(deliveryDistanceTitle)
													.addGap(64)
													.addComponent(deliveryDistanceLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(pizzaTypeTitle)
													.addGap(102)
													.addComponent(pizzaTypeLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(pizzaQuanttyTitle)
													.addGap(84)
													.addComponent(pizzaQuantityLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(orderPriceTitle)
													.addGap(97)
													.addComponent(orderPriceLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(orderCostTitle)
													.addGap(99)
													.addComponent(orderCostLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(orderProfitTitle)
													.addGap(97)
													.addComponent(orderProfitLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(totalProfitTitle)
													.addGap(101)
													.addComponent(totalProfitLabel))
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(totalDistanceTitle)
													.addGap(30)
													.addComponent(totalDistanceLabel))
												.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
													.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
														.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
															.addGap(7)
															.addComponent(comboBox, 0, 259, Short.MAX_VALUE))
														.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
															.addGap(136)
															.addComponent(resetButton, GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE))
														.addGroup(groupLayout.createSequentialGroup()
															.addGap(7)
															.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
																.addComponent(displayButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
																.addComponent(calButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
																.addComponent(prevButton, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
																.addComponent(nextButton, GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE))))
													.addContainerGap())
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(userInputLabel, GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
													.addContainerGap())
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(lblError, GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
													.addContainerGap())
										);
										groupLayout.setVerticalGroup(
											groupLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(groupLayout.createSequentialGroup()
													.addGap(7)
													.addComponent(lblLogFileViewer)
													.addGap(10)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(pizzaOrderIndexTitle)
														.addComponent(pizzaOrderIndexLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(nameTitle)
														.addComponent(nameLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(mobileTitle)
														.addComponent(mobileLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(deliveryTypeTitle)
														.addComponent(deliveryTypeLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(locationXTitle)
														.addComponent(locationXLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(locationYTitle)
														.addComponent(locationYLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(deliveryDistanceTitle)
														.addComponent(deliveryDistanceLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(pizzaTypeTitle)
														.addComponent(pizzaTypeLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(pizzaQuanttyTitle)
														.addComponent(pizzaQuantityLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(orderPriceTitle)
														.addComponent(orderPriceLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(orderCostTitle)
														.addComponent(orderCostLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(orderProfitTitle)
														.addComponent(orderProfitLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(totalProfitTitle)
														.addComponent(totalProfitLabel))
													.addGap(8)
													.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addComponent(totalDistanceTitle)
														.addComponent(totalDistanceLabel))
													.addGap(4)
													.addComponent(nextButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(4)
													.addComponent(prevButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(4)
													.addComponent(calButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(4)
													.addComponent(displayButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(4)
													.addComponent(resetButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(4)
													.addComponent(lblError)
													.addGap(4)
													.addComponent(userInputLabel)
													.addGap(4)
													.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
													.addGap(7))
										);
										frame.getContentPane().setLayout(groupLayout);
	}

	public void checkNextIndex() {
		if (hasFile == true) {
			if (currentIndex + 1 == restaurant.getNumCustomerOrders()) {
				currentIndex = 0;
				displayValues(currentIndex);

			} else {
				currentIndex += 1;
				displayValues(currentIndex);
			}
		} else {
			lblError.setText("Error: No File has been selected");
		}

	}

	public void checkPrevIndex() {
		if (hasFile == true) {
			if (currentIndex == 0) {
				currentIndex = restaurant.getNumCustomerOrders() - 1;
				displayValues(currentIndex);
			} else {
				currentIndex -= 1;
				displayValues(currentIndex);
			}
		} else {
			lblError.setText("Error: No File has been selected");
		}
	}

	public void calTotal() {
		double totalDistance = restaurant.getTotalDeliveryDistance();
		totalDistanceLabel.setText(String.valueOf(totalDistance));
		double totalProfit = restaurant.getTotalProfit();
		totalProfitLabel.setText(String.valueOf(totalProfit));

	}

	public void displayValues(int index) {
		String error = "";

		try {
			pizza = restaurant.getPizzaByIndex(index);
			customer = restaurant.getCustomerByIndex(index);
			pizzaOrderIndexLabel.setText(String.valueOf(index + 1));
			nameLabel.setText(String.valueOf(customer.getName()));
			mobileLabel.setText(String.valueOf(customer.getMobileNumber()));
			deliveryTypeLabel.setText(String.valueOf(customer.getCustomerType()));
			locationXLabel.setText(String.valueOf(customer.getLocationX()));
			locationYLabel.setText(String.valueOf(customer.getLocationY()));
			deliveryDistanceLabel.setText(String.valueOf(customer.getDeliveryDistance()));
			pizzaTypeLabel.setText(String.valueOf(pizza.getPizzaType()));
			pizzaQuantityLabel.setText(String.valueOf(pizza.getQuantity()));
			orderPriceLabel.setText(String.valueOf(pizza.getOrderPrice()));
			orderCostLabel.setText(String.valueOf(pizza.getOrderCost()));
			orderProfitLabel.setText(String.valueOf(pizza.getOrderProfit()));
			nextButton.setEnabled(true);
			prevButton.setEnabled(true);
			calButton.setEnabled(true);

		} catch (Exception e) {
			error = e.getMessage();
		}
		if (error != null) {
			lblError.setText("Error: " + error);
		}

	}

	public void clearValues() {
		lblError.setText("Please select a log file:");
		pizzaOrderIndexLabel.setText("--");
		nameLabel.setText("--");
		mobileLabel.setText("--");
		deliveryTypeLabel.setText("--");
		locationXLabel.setText("--");
		locationYLabel.setText("--");
		deliveryDistanceLabel.setText("--");
		pizzaTypeLabel.setText("--");
		pizzaQuantityLabel.setText("--");
		orderPriceLabel.setText("--");
		orderCostLabel.setText("--");
		orderProfitLabel.setText("--");
		totalProfitLabel.setText("--");
		totalDistanceLabel.setText("--");
		nextButton.setEnabled(false);
		prevButton.setEnabled(false);
		calButton.setEnabled(false);
		displayButton.setEnabled(false);
		resetButton.setEnabled(false);
		frame.setTitle("Pick a Log file");
		comboBox.setSelectedIndex(0); 

	}
}
