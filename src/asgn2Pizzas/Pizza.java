package asgn2Pizzas;

import asgn2Exceptions.PizzaException;

import java.time.LocalTime;
import java.util.ArrayList;

/**
 * An abstract class that represents pizzas sold at the Pizza Palace restaurant.
 * The Pizza class is used as a base class of VegetarianPizza, MargheritaPizza
 * and MeatLoversPizza. Each of these subclasses have a different set of
 * toppings. A description of the class's fields and their constraints is
 * provided in Section 5.1 of the Assignment Specification.
 *
 * @author Samuel Henry
 */
public abstract class Pizza {
	private int pizzaQuantity;
	private LocalTime pizzaOrderTime;
	private LocalTime pizzaDeliveryTime;
	private String pizzaType;
	private double pizzaPrice;
	private double costOfPizza;
	protected ArrayList<PizzaTopping> ingredients = new ArrayList<PizzaTopping>();

	/**
	 * This class represents a pizza produced at the Pizza Palace restaurant. A
	 * detailed description of the class's fields and parameters is provided in
	 * the Assignment Specification, in particular in Section 5.1. A
	 * PizzaException is thrown if the any of the constraints listed in Section
	 * 5.1 of the Assignment Specification are violated.
	 * <p>
	 * PRE: TRUE POST: All field values except cost per pizza are set
	 *
	 * @param quantity
	 *            - The number of pizzas ordered
	 * @param orderTime
	 *            - The time that the pizza order was made and sent to the
	 *            kitchen
	 * @param deliveryTime
	 *            - The time that the pizza was delivered to the customer
	 * @param type
	 *            - A human understandable description of this Pizza type
	 * @param price
	 *            - The price that the pizza is sold to the customer
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 */
	public Pizza(int quantity, LocalTime orderTime, LocalTime deliveryTime, String type, double price)
			throws PizzaException {
		// Tests
		int orderTimeSeconds = (orderTime.getHour() * 3600) + (orderTime.getMinute() * 60) + (orderTime.getSecond());
		int deliveryTimeSeconds = (deliveryTime.getHour() * 3600) + (deliveryTime.getMinute() * 60)
				+ (deliveryTime.getSecond());
		// check if quantity is an amount more than zero pizzas
		if (quantity < 1) {
			throw new PizzaException("You need at least one pizza to be valid: " + quantity + " Must be 1 or more");
		}
		// check if quantity is more than 10, the pizza shop only lets there be
		// 10 pizzas in an order
		if (quantity > 10) {
			throw new PizzaException("Too many pizza for one order: " + quantity + " must be 10 or less");
		}
		// Check if pizza is of one of the three types of pizza
		if (!(type.equals("Margherita")) && !(type.equals("Meat Lovers")) && !(type.equals("Vegetarian"))) {
			throw new PizzaException("Not valid type:  " + type);
		}
		// check if the pizza is no longer warm enough
		if (deliveryTimeSeconds - orderTimeSeconds >= 3600) {
			throw new PizzaException("Pizza is over an hour old and must be thrown out: "
					+ (deliveryTimeSeconds - orderTimeSeconds) + "needs to be less then 3600 ");
		}
		// check if a pizza is being delivered before it's ordered
		if (orderTime.isAfter(deliveryTime)) {
			throw new PizzaException("Pizza can't be delivered or picked up as it hasn't been ordered yet");
		}
		// check that the pizza is finished cooking
		if (deliveryTimeSeconds - orderTimeSeconds < 600) {
			throw new PizzaException(
					"Pizza isn't cooked yet: " + (deliveryTimeSeconds - orderTimeSeconds) + "needs to be 600");
		}
		// check that the pizza shop is open
		if (orderTimeSeconds < 68400) {
			throw new PizzaException("the pizza shop opens at 7pm: " + orderTimeSeconds + ": opens at 14400");
		}
		// pizza can be delivered for an hour after the shop shuts
		if (deliveryTimeSeconds >= 86400) {
			throw new PizzaException(
					"the day is over so pizza can't be deliverd: " + deliveryTimeSeconds + ": shuts  at 86400");
		}

		// check to see if an order was placed after the store shut
		if (orderTimeSeconds >= 82800) {
			throw new PizzaException("Shop has shut: " + orderTimeSeconds + ": stops at 82800");
		}
		// the pizza need to have a price which is a positive number
		if (price <= 0) {
			throw new PizzaException("Price is too cheap: " + price);
		}

		ingredients.clear();
		this.pizzaQuantity = quantity;
		this.pizzaOrderTime = orderTime;
		this.pizzaDeliveryTime = deliveryTime;
		this.pizzaType = type;
		this.pizzaPrice = price;
	}

	/**
	 * Calculates how much a pizza would cost to make calculated from its
	 * toppings.
	 * <p>
	 * <P>
	 * PRE: TRUE
	 * <P>
	 * POST: The cost field is set to sum of the Pizzas's toppings
	 */
	private void calculateCostPerPizza() {
		this.costOfPizza = 0;
		for (PizzaTopping topping : ingredients) {
			this.costOfPizza += topping.getCost();
		}
	}

	/**
	 * Returns the amount that an individual pizza costs to make.
	 *
	 * @return The amount that an individual pizza costs to make.
	 */
	public final double getCostPerPizza() {
		calculateCostPerPizza();
		return this.costOfPizza;

	}

	/**
	 * Returns the amount that an individual pizza is sold to the customer.
	 *
	 * @return The amount that an individual pizza is sold to the customer.
	 */
	public final double getPricePerPizza() {
		return this.pizzaPrice;
	}

	/**
	 * Returns the amount that the entire order costs to make, taking into
	 * account the type and quantity of pizzas.
	 *
	 * @return The amount that the entire order costs to make, taking into
	 *         account the type and quantity of pizzas.
	 */
	public final double getOrderCost() {
		double costPerPizza = getCostPerPizza();
		double quantity = getQuantity();
		return costPerPizza * quantity;
	}

	/**
	 * Returns the amount that the entire order is sold to the customer, taking
	 * into account the type and quantity of pizzas.
	 *
	 * @return The amount that the entire order is sold to the customer, taking
	 *         into account the type and quantity of pizzas.
	 */
	public final double getOrderPrice() {
		double pricePerPizza = this.getPricePerPizza();
		double quantity = getQuantity();
		return pricePerPizza * quantity;
	}

	/**
	 * Returns the profit made by the restaurant on the order which is the order
	 * price minus the order cost.
	 *
	 * @return Returns the profit made by the restaurant on the order which is
	 *         the order price minus the order cost.
	 */
	public final double getOrderProfit() {
		double cost = getOrderCost();
		double price = getOrderPrice();
		return price - cost;
	}

	/**
	 * Indicates if the pizza contains the specified pizza topping or not.
	 *
	 * @param topping
	 *            - A topping as specified in the enumeration PizzaTopping
	 * @return Returns true if the instance of Pizza contains the specified
	 *         topping and false otherwise.
	 */
	public final boolean containsTopping(PizzaTopping topping) {
		return this.ingredients.contains(topping);
	}

	/**
	 * Returns the quantity of pizzas ordered.
	 *
	 * @return the quantity of pizzas ordered.
	 */
	public final int getQuantity() {
		return this.pizzaQuantity;
	}

	/**
	 * Returns a human understandable description of the Pizza's type. The valid
	 * alternatives are listed in Section 5.1 of the Assignment Specification.
	 *
	 * @return A human understandable description of the Pizza's type.
	 */
	public final String getPizzaType() {
		return this.pizzaType;
	}

	/**
	 * Compares *this* Pizza object with an instance of an *other* Pizza object
	 * and returns true if if the two objects are equivalent, that is, if the
	 * values exposed by public methods are equal. You do not need to test this
	 * method.
	 *
	 * @return true if *this* Pizza object and the *other* Pizza object have the
	 *         same values returned for getCostPerPizza(), getOrderCost(),
	 *         getOrderPrice(), getOrderProfit(), getPizzaType(),
	 *         getPricePerPizza() and getQuantity().
	 */
	@Override
	public boolean equals(Object other) {
		Pizza otherPizza = (Pizza) other;
		return ((this.getCostPerPizza()) == (otherPizza.getCostPerPizza())
				&& (this.getOrderCost()) == (otherPizza.getOrderCost()))
				&& (this.getOrderPrice()) == (otherPizza.getOrderPrice())
				&& (this.getOrderProfit()) == (otherPizza.getOrderProfit())
				&& (this.getPizzaType() == (otherPizza.getPizzaType())
						&& (this.getPricePerPizza()) == (otherPizza.getPricePerPizza())
						&& (this.getQuantity()) == (otherPizza.getQuantity()));
	}

}
