package asgn2Tests;
import static org.junit.Assert.assertTrue;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.DriverDeliveryCustomer;
import asgn2Pizzas.*;
import asgn2Restaurant.LogHandler;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.PizzaException;
import asgn2Exceptions.LogHandlerException;

/**
 * A class that tests the methods relating to the creation of Pizza objects in
 * the asgn2Restaurant.LogHander class.
 * 
 * @author Hayden Muller
 * 
 */

public class LogHandlerPizzaTests {
	Pizza testPizza;
	Pizza testMeatLoversPizza;
	
	
	@Before 
	public void setup() throws Exception{
		
	}
	
	// Tests that a MeatLovers pizza is made correctly 
	@Test
	public void createPizzaObjectTest() throws Exception {
		String line = "19:20:00,19:40:00,Bindu,0123456789,DVC,-3,-2,PZM,3";
		testPizza = LogHandler.createPizza(line);
		testMeatLoversPizza = new MeatLoversPizza(3,LocalTime.of(19,20), LocalTime.of(19,30));
		assertTrue(testMeatLoversPizza.equals(testPizza));
	}
	// Tests that an exception is thrown when a list that is too large is given.
	@Test(expected = LogHandlerException.class)
	public void outOFRangeTest() throws Exception {
		String line = "100,19:20:00,19:40:00, Bindu ,0123456789,DVC,-3,-2,PZM,3";
		testPizza = LogHandler.createPizza(line);
	}
	// Tests that an exception is thrown when the quantity is 0.
	@Test(expected = PizzaException.class)
	public void noQuantityTest() throws Exception {
		String line = "20:00:00,19:40:00, Bindu ,0123456789,DVC,-3,-2,PZM,0";
		testPizza = LogHandler.createPizza(line);
	}
	@Test(expected = LogHandlerException.class)
	public void createPizzaLogExceptionTest() throws Exception {
		String line = "Invalid";
		testPizza = LogHandler.createPizza(line);
	}
	// TO DO
}
