package asgn2Tests;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.*;
import static org.junit.Assert.*;
import asgn2Customers.Customer;
import asgn2Exceptions.*;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;
/**
 * A class that tests the methods relating to the handling of Pizza objects in
 * the asgn2Restaurant.PizzaRestaurant class as well as processLog and
 * resetDetails.
 * 
 * @author Hayden Muller
 *
 */
public class RestaurantPizzaTests {
	ArrayList<Pizza> pizza;
	MeatLoversPizza meatLoversPizzaTest1;

	
	
	@Before
	public void setup() throws Exception{
	
	this.meatLoversPizzaTest1 = new MeatLoversPizza(1, LocalTime.of(20, 00, 00), LocalTime.of(20, 25, 00));

	
	}
	
	//Tests if a log file with a quantity error throws a PizzaException through .
	@Test (expected = PizzaException.class)
	public void pizzaExceptionTest() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("quantityTooHigh.txt");
	}
	
	//Tests if a blank log file throws a logHandlerException.
	@Test (expected = LogHandlerException.class)
	public void logHandlerExceptionTest() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("blankLog.txt");
	}
	
	//Tests if .getPizzaByIndex returns the correct pizza.
	@Test 
	public void getPizzaByIndexTest() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		
		testRestaurant.processLog("20170101.txt");
		Pizza testPizza = testRestaurant.getPizzaByIndex(1);
		assertTrue(testPizza.equals(meatLoversPizzaTest1));
	}
	
	//Tests if .getNumPizzaOrders returns the correct number of pizza's in an order 
	@Test
	public void getNumPizzaOrdersTest() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("20170102.txt");
		int testOrders = testRestaurant.getNumPizzaOrders(); 
		assertEquals(10, testOrders);
	}
	
	//Tests if .getTotalProfit returns the correct amount of profit
	@Test
	public void getTotalProfittest() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("20170102.txt");
		double testTotalProfit = testRestaurant.getTotalProfit();
		double answer = 300;
		assertTrue(answer == testTotalProfit);
	}
	
	//Tests that the reset test function actually resets the restaurant
	@Test
	public void resetTest() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("20170102.txt");
		testRestaurant.resetDetails();
		int orders = testRestaurant.getNumPizzaOrders();
		assertEquals(0, orders);
	}
	
	//Tests that if the log file doesn't exist it throws an error.
	@Test (expected = LogHandlerException.class)
	public void noLogTestException() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("log101.txt");
	}
	
	//Tests that an exception is thrown if there is an incorrect number of items on a row
	@Test (expected = LogHandlerException.class)
	public void numberOfItemsTest() throws Exception {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("error.txt");
	}
}
