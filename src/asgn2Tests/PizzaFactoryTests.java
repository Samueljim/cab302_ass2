package asgn2Tests;

import static org.junit.Assert.*;

import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import asgn2Customers.CustomerFactory;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.*;

/**
 * A class that tests the asgn2Pizzas.PizzaFactory class.
 * 
 * @author Hayden Muller
 * 
 */
public class PizzaFactoryTests {
	private Pizza testPizza;
	private MeatLoversPizza testMeatLoversPizza;
	private MargheritaPizza testMargheritaPizza;
	private VegetarianPizza testVegetarianPizza;
	
	// TO DO
	@Before
	public void setup() throws Exception{
		
	}
	
	//Tests for an exception when Margherita type is passed into getPizza
	@Test (expected = PizzaException.class)
	public void invalidTypeTest1() throws Exception{
		PizzaFactory.getPizza("Margherita", 1, LocalTime.of(19,20), LocalTime.of(19,30));
	}
	//Tests for an exception when Meat Lovers type is passed into getPizza
	@Test (expected = PizzaException.class)
	public void invalidTypeTest2() throws Exception{
		PizzaFactory.getPizza("Meat Lovers", 1, LocalTime.of(19,20), LocalTime.of(19,30));
	}
	//Tests for an exception when Vegetarian type is passed into getPizza
	@Test (expected = PizzaException.class)
	public void invalidTypeTest3() throws Exception{
		PizzaFactory.getPizza("Vegetarian", 1, LocalTime.of(19,20), LocalTime.of(19,30));
	}
	//Tests for an exception when a three character type is passed into getPizza
	@Test (expected = PizzaException.class)
	public void invalidTypeTest4() throws Exception{
		PizzaFactory.getPizza("BSB", 1, LocalTime.of(19,20), LocalTime.of(19,30));
	}
	//Tests that the MeatLovers type is created properly
	@Test
	public void returnMeatLoversTest() throws Exception {
		testPizza = PizzaFactory.getPizza("PZM", 1, LocalTime.of(19,20), LocalTime.of(19,30));
		testMeatLoversPizza = new MeatLoversPizza(1, LocalTime.of(19,20), LocalTime.of(19,30));
		assertTrue(testMeatLoversPizza.equals(testPizza));
	}
	//Tests that the Margherita type is created properly 
	@Test
	public void returnMargheritaTest() throws Exception {
		testPizza = PizzaFactory.getPizza("PZL", 1, LocalTime.of(19,20), LocalTime.of(19,30));
		testMargheritaPizza = new MargheritaPizza(1, LocalTime.of(19,20), LocalTime.of(19,30));
		assertTrue(testMargheritaPizza.equals(testPizza));
	}
	//Tests that the Vegetarian type is created properly 
	@Test
	public void returnVegetarianTest() throws Exception {
		testPizza = PizzaFactory.getPizza("PZV", 1, LocalTime.of(19,20), LocalTime.of(19,30));
		testVegetarianPizza = new VegetarianPizza(1, LocalTime.of(19,20), LocalTime.of(19,30));
		assertTrue(testVegetarianPizza.equals(testPizza));
	}
	
	
	
}
