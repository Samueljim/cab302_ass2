package asgn2Pizzas;

import java.time.LocalTime;

import asgn2Exceptions.PizzaException;

/**
 *
 * A class that represents a vegetarian pizza made at the Pizza Palace
 * restaurant. The vegetarian pizza has certain toppings listed in Section 5.1
 * of the Assignment Specification Document. A description of the class's fields
 * and their constraints is provided in Section 5.1 of the Assignment
 * Specification.
 *
 * @author Samuel Henry
 *
 */
public class VegetarianPizza extends Pizza {
	private static double priceOfPizza = 10;
	private static String pizzaType = "Vegetarian";

	/**
	 *
	 * This class represents a vegetarian pizza made at the Pizza Palace
	 * restaurant. The vegetarian pizza has certain toppings listed in Section
	 * 5.1 of the Assignment Specification Document. A description of the
	 * class's fields and their constraints is provided in Section 5.1 of the
	 * Assignment Specification. A PizzaException is thrown if the any of the
	 * constraints listed in Section 5.1 of the Assignment Specification are
	 * violated.
	 *
	 * <P>
	 * PRE: TRUE
	 * <P>
	 * POST: All field values including the cost per pizza are set
	 * 
	 * @param quantity
	 *            - The number of pizzas ordered
	 * @param orderTime
	 *            - The time that the pizza order was made and sent to the
	 *            kitchen
	 * @param deliveryTime
	 *            - The time that the pizza was delivered to the customer
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 *
	 */
	public VegetarianPizza(int quantity, LocalTime orderTime, LocalTime deliveryTime) throws PizzaException {
		super(quantity, orderTime, deliveryTime, pizzaType, priceOfPizza);

		ingredients.add(PizzaTopping.CHEESE);
		ingredients.add(PizzaTopping.TOMATO);
		ingredients.add(PizzaTopping.EGGPLANT);
		ingredients.add(PizzaTopping.MUSHROOM);
		ingredients.add(PizzaTopping.CAPSICUM);

		// Check if the right toppings are of a on this pizza
		if (containsTopping(PizzaTopping.SALAMI) || containsTopping(PizzaTopping.PEPPERONI)
				|| containsTopping(PizzaTopping.BACON)) {
			throw new PizzaException("There is meat on this pizza");
		}
	}

}
