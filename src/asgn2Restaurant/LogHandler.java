package asgn2Restaurant;

import java.time.LocalTime;
import java.util.ArrayList;
import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory; // added this
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 *
 * A class that contains methods that use the information in the log file to
 * return Pizza and Customer object - either as an individual Pizza/Customer
 * object or as an ArrayList of Pizza/Customer objects.
 * 
 * @author Samuel Henry and Hayden Muller
 *
 */
public class LogHandler {
	private final static String COMMA = ",";

	/**
	 * Returns an ArrayList of Customer objects from the information contained
	 * in the log file ordered as they appear in the log file.
	 * 
	 * @param filename
	 *            The file name of the log file
	 * @return an ArrayList of Customer objects from the information contained
	 *         in the log file ordered as they appear in the log file.
	 * @throws CustomerException
	 *             If the log file contains semantic errors leading that violate
	 *             the customer constraints listed in Section 5.3 of the
	 *             Assignment Specification or contain an invalid customer code
	 *             (passed by another class).
	 * @throws LogHandlerException
	 *             If there was a problem with the log file not related to the
	 *             semantic errors above
	 * 
	 */
	public static ArrayList<Customer> populateCustomerDataset(String filename)
			throws CustomerException, LogHandlerException {

		ArrayList<Customer> arraylist = new ArrayList<Customer>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("./logs/"+filename));
			
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				arraylist.add(createCustomer(line));
			}
			br.close();
		} catch (CustomerException e) {
			throw new CustomerException(e.getMessage());
		} catch (Exception e){
			throw new LogHandlerException(e.getMessage());
		}
		
		return arraylist;
	}
	
	/**
	 * Returns an ArrayList of Pizza objects from the information contained in
	 * the log file ordered as they appear in the log file. .
	 * 
	 * @param filename
	 *            The file name of the log file
	 * @return an ArrayList of Pizza objects from the information contained in
	 *         the log file ordered as they appear in the log file. .
	 * @throws PizzaException
	 *             If the log file contains semantic errors leading that violate
	 *             the pizza constraints listed in Section 5.3 of the Assignment
	 *             Specification or contain an invalid pizza code (passed by
	 *             another class).
	 * @throws LogHandlerException
	 *             If there was a problem with the log file not related to the
	 *             semantic errors above
	 * 
	 */
	public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException {
		// 0 order-time, 1 delivery-time, 2 customer-name, 3 customer-mobile, 4
		// customer-code, 5 customer-x-location, 6 customer-y-location, 7
		// pizza-code, 8 pizza-quantity
		//String path = System.getProperty("user.dir") + "//logs";
		//File data = new File(path, filename);
		
		
		ArrayList<Pizza> arraylist = new ArrayList<Pizza>();
		
		try {
			BufferedReader br = new BufferedReader(new FileReader("./logs/"+filename));
			
			for (String line = br.readLine(); line != null; line = br.readLine()) {
				arraylist.add(createPizza(line));
			}
			br.close();
			
		} catch (PizzaException e) {
			throw new PizzaException(e.getMessage());
		} catch (Exception e) {
			throw new LogHandlerException(e.getMessage());
		}
		
		return arraylist;
	}

	/**
	 * Creates a Customer object by parsing the information contained in a
	 * single line of the log file. The format of each line is outlined in
	 * Section 5.3 of the Assignment Specification.
	 * 
	 * @param line
	 *            - A line from the log file
	 * @return- A Customer object containing the information from the line in
	 *          the log file
	 * @throws CustomerException
	 *             - If the log file contains semantic errors leading that
	 *             violate the customer constraints listed in Section 5.3 of the
	 *             Assignment Specification or contain an invalid customer code
	 *             (passed by another class).
	 * @throws LogHandlerException
	 *             - If there was a problem parsing the line from the log file.
	 */
	public static Customer createCustomer(String line) throws CustomerException, LogHandlerException {
		
		Customer man;
			String customerCode;
			String name;
			String mobile;
			int locationX;
			int locationY;
			//Set the types being stored to their correct format
			try {
				String[] fileOutput = line.split(COMMA);
				customerCode = String.valueOf(fileOutput[4]);
				name = String.valueOf(fileOutput[2]);
				mobile = String.valueOf(fileOutput[3]);
				locationX = Integer.parseInt(fileOutput[5]);
				locationY = Integer.parseInt(fileOutput[6]);
				
			} catch (Exception e) {
				throw new LogHandlerException("LogHandler Problem: " + e.getMessage());
			}
			try {
				man = CustomerFactory.getCustomer(customerCode, name, mobile, locationX, locationY);
			} catch (Exception e) {
				throw e;
			}
			return man;
			
		}
	

	/**
	 * Creates a Pizza object by parsing the information contained in a single
	 * line of the log file. The format of each line is outlined in Section 5.3
	 * of the Assignment Specification.
	 * 
	 * @param line
	 *            - A line from the log file
	 * @return- A Pizza object containing the information from the line in the
	 *          log file
	 * @throws PizzaException
	 *             If the log file contains semantic errors leading that violate
	 *             the pizza constraints listed in Section 5.3 of the Assignment
	 *             Specification or contain an invalid pizza code (passed by
	 *             another class).
	 * @throws LogHandlerException
	 *             - If there was a problem parsing the line from the log file.
	 */
	public static Pizza createPizza(String line) throws PizzaException, LogHandlerException {
		Pizza pie;
		String type;
		int quantity;
		LocalTime orderTime;
		LocalTime deliveryTime;
		//Set the types being stored to their correct format
		try {
			String[] fileOutput = line.split(COMMA);
			orderTime = LocalTime.parse(fileOutput[0]);
			deliveryTime = LocalTime.parse(fileOutput[1]);
			type = String.valueOf(fileOutput[7]);
			quantity = Integer.parseInt(fileOutput[8]);
		} catch (Exception e) {
			throw new LogHandlerException("LogHandler Problem: " + e.getMessage());
		}
		try {
			pie = PizzaFactory.getPizza(type, quantity, orderTime, deliveryTime);
		} catch (Exception e) {
			throw e;
		}
		return pie;
		
	}

}
