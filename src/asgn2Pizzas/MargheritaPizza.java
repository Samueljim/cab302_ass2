package asgn2Pizzas;

import java.time.LocalTime;

import asgn2Exceptions.PizzaException;

/**
 *
 * A class that represents a margherita pizza made at the Pizza Palace
 * restaurant. The margherita pizza has certain toppings listed in Section 5.1
 * of the Assignment Specification Document. A description of the class's fields
 * and their constraints is provided in Section 5.1 of the Assignment
 * Specification.
 *
 * @author Samuel Henry
 *
 */
public class MargheritaPizza extends Pizza {
	private static double priceOfPizza = 8;
	private static String pizzaType = "Margherita";

	/**
	 *
	 * This class represents a margherita pizza made at the Pizza Palace
	 * restaurant. The margherita pizza has certain toppings listed in Section
	 * 5.1 of the Assignment Specification Document. A description of the
	 * class's fields and their constraints is provided in Section 5.1 of the
	 * Assignment Specification. A PizzaException is thrown if the any of the
	 * constraints listed in Section 5.1 of the Assignment Specification are
	 * violated.
	 *
	 * <P>
	 * PRE: TRUE
	 * <P>
	 * POST: All field values including the cost per pizza are set
	 *
	 * @param quantity
	 *            - The number of pizzas ordered
	 * @param orderTime
	 *            - The time that the pizza order was made and sent to the
	 *            kitchen
	 * @param deliveryTime
	 *            - The time that the pizza was delivered to the customer
	 * @throws PizzaException
	 *             if supplied parameters are invalid
	 *
	 */
	public MargheritaPizza(int quantity, LocalTime orderTime, LocalTime deliveryTime) throws PizzaException {
		super(quantity, orderTime, deliveryTime, pizzaType, priceOfPizza);

		ingredients.add(PizzaTopping.CHEESE);
		ingredients.add(PizzaTopping.TOMATO);

		// Check if the right toppings are of a on this pizza
		if (containsTopping(PizzaTopping.SALAMI) || containsTopping(PizzaTopping.BACON)
				|| containsTopping(PizzaTopping.PEPPERONI) || containsTopping(PizzaTopping.EGGPLANT)
				|| containsTopping(PizzaTopping.CAPSICUM) || containsTopping(PizzaTopping.MUSHROOM)) {
			throw new PizzaException("Toppings are not right");
		}
	}
}
