/**
 * This package contains the GUI which can be used to interact with the rest of the system. 
 * 
 * @author Samuel Henry and Hayden Muller
 *
 */
package asgn2GUIs;